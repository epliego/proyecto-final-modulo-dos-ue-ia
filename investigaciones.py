import math
from math import acos, sin, cos
import requests
import json

# Definir la URL de la API
url_base = "https://www.crtm.es/widgets/api"


# Función para obtener información de una parada específica
def obtener_parada(parada_id):
    """
    Obtiene información detallada de una parada específica.

    Args:
      parada_id: Identificador único de la parada.

    Returns:
      Diccionario con información de la parada, o None si no se encuentra.
    """
    url = f"{url_base}/GetStops.php?codStop={parada_id}"
    response = requests.get(url)
    if response.status_code == 200:
        return json.loads(response.content)
    else:
        print(f"Error al obtener información de la parada: {parada_id}")
        return None


# Función para obtener la ruta entre dos paradas
def obtener_ruta(parada_origen, parada_destino):
    """
    Obtiene la ruta más corta entre dos paradas.

    Args:
      parada_origen: Identificador de la parada de origen.
      parada_destino: Identificador de la parada de destino.

    Returns:
      Diccionario con información de la ruta, o None si no se encuentra.
    """
    # TODO: Implementar la lógica para obtener la ruta utilizando la API
    # del Consorcio Transportes de Madrid.
    print("Función para obtener ruta aún no implementada.")
    return None


# Función para calcular la distancia entre dos paradas
def calcular_distancia(parada_origen, parada_destino):
    """
    Calcula la distancia en línea recta entre dos paradas.

    Args:
      parada_origen: Diccionario con información de la parada de origen.
      parada_destino: Diccionario con información de la parada de destino.

    Returns:
      Distancia aproximada en kilómetros entre las dos paradas.
    """
    lat_origen = parada_origen["ubicacion"]["latitud"]
    lon_origen = parada_origen["ubicacion"]["longitud"]
    lat_destino = parada_destino["ubicacion"]["latitud"]
    lon_destino = parada_destino["ubicacion"]["longitud"]

    # Fórmula de distancia entre dos puntos en la Tierra (en kilómetros)
    distancia = 6371.01 * acos(
        sin(lat_origen) * sin(lat_destino) + cos(lat_origen) * cos(lat_destino) * cos(lon_destino - lon_origen))
    return distancia


# Ejemplo de uso
parada_id = "4_276"  # Identificador de la parada de ejemplo (Plaza de España)
parada_info = obtener_parada(parada_id)

if parada_info:
    print(f"Información de la parada {parada_id}:")
    print(parada_info)

# TODO: Descomentar y completar la implementación de obtener_ruta y calcular_distancia
# para obtener la ruta y distancia desde la parada de ejemplo.

# ruta_info = obtener_ruta(parada_id, "parada_destino")
# if ruta_info:
#   print(f"Ruta desde {parada_id} a parada_destino:")
#   print(ruta_info)

#   distancia = calcular_distancia(parada_info, ruta_info["paradas_intermedias"][0])
#   print(f"Distancia aproximada: {distancia:.2f} km")
# else:
#   print("No se encontró ruta para el destino especificado.")
else:
    print(f"No se encontró información para la parada: {parada_id}")

###################### Cargar paradas ######################
# Definir la URL de la API
url_base = "https://api.consorciotransporte.madrid.es/v1/metro/estaciones"


# Función para obtener información de una parada específica
def obtener_parada(parada_id):
    """
    Obtiene información detallada de una parada específica.

    Args:
      parada_id: Identificador único de la parada.

    Returns:
      Diccionario con información de la parada, o None si no se encuentra.
    """
    url = f"{url_base}/{parada_id}"
    response = requests.get(url)
    if response.status_code == 200:
        return json.loads(response.content)
    else:
        print(f"Error al obtener información de la parada: {parada_id}")
        return None


# Crear un arreglo para almacenar información de las paradas
paradas = []

# Obtener información de las primeras 10 paradas
for parada_id in range(1, 11):
    parada_info = obtener_parada(parada_id)
    if parada_info:
        paradas.append(parada_info)

# Imprimir información de las paradas obtenidas
for parada in paradas:
    print(f"Parada: {parada['nombre']}")
    print(f"Ubicación: {parada['ubicacion']['latitud']:.4f}, {parada['ubicacion']['longitud']:.4f}")
    print(f"Líneas: {parada['lineas']}")
    print()

# Acceder a la información de una parada específica por índice
parada_seleccionada = paradas[4]  # Seleccionar la quinta parada
print(f"Información de la parada {parada_seleccionada['nombre']}:")
print(parada_seleccionada)


###################### Algoritmo MinMax en Python ######################
class Nodo:
    def __init__(self, posicion, distancia, costo, padre):
        self.posicion = posicion
        self.distancia = distancia
        self.costo = costo
        self.padre = padre


def buscar_ruta_optima(grafo, nodo_inicial, nodo_destino, distancia_maxima, costo_maximo):
    """
    Búsqueda en profundidad con poda alpha-beta para encontrar la ruta óptima entre dos nodos.

    Args:
      grafo: Diccionario que representa el grafo, donde las claves son las posiciones de los nodos y los valores son diccionarios con los nodos adyacentes y sus costos.
      nodo_inicial: Nodo inicial del camino.
      nodo_destino: Nodo destino del camino.
      distancia_maxima: Distancia máxima permitida para la ruta.
      costo_maximo: Costo máximo permitido para la ruta.

    Returns:
      Lista de nodos que forman la ruta óptima, o None si no se encuentra un camino.
    """
    mejor_camino = None
    mejor_costo = float('inf')

    def explorar_nodo(nodo_actual, distancia_acumulada, costo_acumulado):
        nonlocal mejor_camino, mejor_costo

        # Verificar si se ha llegado al nodo destino y si la ruta es mejor que las anteriores
        if nodo_actual.posicion == nodo_destino.posicion and costo_acumulado < mejor_costo:
            mejor_camino = reconstruir_camino(nodo_actual)
            mejor_costo = costo_acumulado
            return

        # Verificar si se supera la distancia o costo máximo
        if distancia_acumulada > distancia_maxima or costo_acumulado > costo_maximo:
            return

        # Explorar nodos adyacentes
        for nodo_adyacente, costo_adyacente in grafo[nodo_actual.posicion].items():
            nueva_distancia = distancia_acumulada + calcular_distancia(nodo_actual.posicion, nodo_adyacente)
            nuevo_costo = costo_acumulado + costo_adyacente

            # Poda alpha-beta: si el costo actual supera el mejor costo encontrado hasta ahora, no hay necesidad de explorar más ramas
            if nuevo_costo >= mejor_costo:
                continue

            explorar_nodo(Nodo(nodo_adyacente, nueva_distancia, nuevo_costo, nodo_actual), nueva_distancia, nuevo_costo)

    explorar_nodo(nodo_inicial, 0, 0)
    return mejor_camino


def reconstruir_camino(nodo_actual):
    """
    Reconstruye el camino desde el nodo actual hasta el nodo inicial.

    Args:
      nodo_actual: Nodo final del camino.

    Returns:
      Lista de nodos que forman el camino, comenzando por el nodo inicial.
    """
    camino = []
    while nodo_actual:
        camino.append(nodo_actual)
        nodo_actual = nodo_actual.padre
    camino.reverse()
    return camino


def calcular_distancia(posicion1, posicion2):
    # Implementar la lógica para calcular la distancia entre dos posiciones
    # (por ejemplo, distancia euclidiana o distancia en un mapa)
    pass


###################### Algoritmo distancia euclidiana Python ######################
def calcular_distancia_euclidiana(punto1, punto2):
    """
    Calcula la distancia euclidiana entre dos puntos en el plano.

    Args:
      punto1: Tupla con las coordenadas (x1, y1) del primer punto.
      punto2: Tupla con las coordenadas (x2, y2) del segundo punto.

    Returns:
      Distancia euclidiana entre los dos puntos.
    """
    (x1, y1) = punto1
    (x2, y2) = punto2

    # Fórmula de la distancia euclidiana
    distancia = math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
    return distancia


# Ejemplo de uso
punto1 = (3, 4)
punto2 = (5, 11)

distancia = calcular_distancia_euclidiana(punto1, punto2)
print(f"Distancia euclidiana entre {punto1} y {punto2}: {distancia:.2f}")

# https://github.com/jvicentem/citram-python-api/tree/master
# https://data-crtm.opendata.arcgis.com/datasets/bases-3cbike/api
