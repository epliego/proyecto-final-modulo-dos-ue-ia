import networkx as nx
import matplotlib.pyplot as plt

# Definir los nodos como diccionarios con información de posición y nombre
nodos = {
    "1": {"posicion": (40.4167, -3.7033), "nombre": "Plaza de España"},
    "2": {"posicion": (40.4175, -3.7008), "nombre": "Gran Vía"},
    "3": {"posicion": (40.4242, -3.6933), "nombre": "Sol"},
    "4": {"posicion": (40.4258, -3.6900), "nombre": "Puerta del Sol"},
    "5": {"posicion": (40.4267, -3.6867), "nombre": "Calle de Alcalá"},
}

# Crear el grafo utilizando networkx
grafo = nx.Graph()

# Agregar nodos al grafo
grafo.add_nodes_from(nodos.keys())

# Agregar aristas con información de distancia y costo
grafo.add_edges_from([
    ("1", "2", {"distancia": 0.4, "costo": 1.5}),
    ("1", "3", {"distancia": 0.6, "costo": 1.8}),
    ("2", "3", {"distancia": 0.3, "costo": 1.2}),
    ("2", "4", {"distancia": 0.5, "costo": 1.7}),
    ("3", "4", {"distancia": 0.2, "costo": 1.0}),
    ("3", "5", {"distancia": 0.7, "costo": 2.0}),
    ("4", "5", {"distancia": 0.4, "costo": 1.4}),
])

# Visualizar el grafo (opcional)
#nx.draw(grafo, labels=nodos, with_labels=True, node_size=500, node_color='skyblue', edge_color='black')
#plt.show()

# Obtener información de las aristas
for nodo_origen, nodo_destino, data in grafo.edges(data=True):
    distancia = data["distancia"]
    costo = data["costo"]
    print(f"Distancia entre {nodos[nodo_origen]['nombre']} y {nodos[nodo_destino]['nombre']}: {distancia} km")
    print(f"Costo entre {nodos[nodo_origen]['nombre']} y {nodos[nodo_destino]['nombre']}: {costo} €")
    print()

# Calcular la ruta más corta entre dos nodos
nodo_inicial = "1"
nodo_destino = "5"
ruta_corta = nx.shortest_path(grafo, source=nodo_inicial, target=nodo_destino, weight="distancia")
distancia_total = nx.shortest_path_length(grafo, source=nodo_inicial, target=nodo_destino, weight="distancia")
costo_total = nx.shortest_path_length(grafo, source=nodo_inicial, target=nodo_destino, weight="costo")

costo_economico = nx.shortest_path(grafo, source=nodo_inicial, target=nodo_destino, weight="costo")
distancia_costo_economico_total = nx.shortest_path_length(grafo, source=nodo_inicial, target=nodo_destino, weight="distancia")

print(f"Ruta más corta entre {nodos[nodo_inicial]['nombre']} y {nodos[nodo_destino]['nombre']}:")
for i in range(len(ruta_corta) - 1):
    nodo_actual = ruta_corta[i]
    nodo_siguiente = ruta_corta[i + 1]
    distancia = grafo[nodo_actual][nodo_siguiente]["distancia"]
    costo = grafo[nodo_actual][nodo_siguiente]["costo"]
    print(f"  - Ir de {nodos[nodo_actual]['nombre']} a {nodos[nodo_siguiente]['nombre']} ({distancia} km, {costo} €)")

print(f"Distancia total: {distancia_total} km")
print(f"Costo total: {costo_total} €")
print()

print(f"Ruta más económica entre {nodos[nodo_inicial]['nombre']} y {nodos[nodo_destino]['nombre']}:")
for i in range(len(costo_economico) - 1):
    nodo_actual = costo_economico[i]
    nodo_siguiente = costo_economico[i + 1]
    distancia = grafo[nodo_actual][nodo_siguiente]["distancia"]
    costo = grafo[nodo_actual][nodo_siguiente]["costo"]
    print(f"  - Ir de {nodos[nodo_actual]['nombre']} a {nodos[nodo_siguiente]['nombre']} ({distancia} km, {costo} €)")

print(f"Distancia total: {distancia_costo_economico_total} km")
print(f"Costo total: {costo_total} €")
print()

# Dibujar el recorrido anterior entre los nodos
grafo_ruta_corta = grafo.edge_subgraph(zip(ruta_corta, ruta_corta[1:]))
posicion_nodos = nx.spring_layout(grafo)
nx.draw_networkx(grafo, posicion_nodos, with_labels=True, node_size=500, node_color="lightblue")
nx.draw_networkx_edges(grafo, posicion_nodos, edgelist=grafo_ruta_corta.edges(), edge_color="red", width=2)
plt.show()
