import networkx as nx
import matplotlib.pyplot as plt

# Repositorio en GitLab: https://gitlab.com/epliego/proyecto-final-modulo-dos-ue-ia

# Definir los nodos como diccionarios con información de posición y nombre
# nodos = {
# 	"1": {"posicion": (40.4167, -3.7033), "nombre": "Plaza de España"},
# 	"2": {"posicion": (40.4175, -3.7008), "nombre": "Gran Vía"},
# 	"3": {"posicion": (40.4242, -3.6933), "nombre": "Sol"},
# 	"4": {"posicion": (40.4258, -3.6900), "nombre": "Puerta del Sol"},
# 	"5": {"posicion": (40.4267, -3.6867), "nombre": "Calle de Alcalá"},
# }
nodos = {
	"1": {"posicion": (40.41686558724001, -3.703696447690377), "nombre": "Sol"},
	"2": {"posicion": (40.419930389049505, -3.7016510878411735), "nombre": "Gran Vía"},
	"3": {"posicion": (40.42615496367381, -3.701220746561205), "nombre": "Tribunal"},
	"4": {"posicion": (40.42386082422385, -3.7108940636685723), "nombre": "Plaza de España"},
	"5": {"posicion": (40.42026926036084, -3.7057461388431676), "nombre": "Callao"},
}

# Crear el grafo utilizando networkx
grafo = nx.Graph()

# Agregar nodos al grafo
grafo.add_nodes_from(nodos.keys())

# Agregar aristas con información de distancia y costo
grafo.add_edges_from([
	("1", "2", {"distancia": 0.4, "costo": 1.5}),
	("1", "3", {"distancia": 0.6, "costo": 1.8}),
	("2", "3", {"distancia": 0.3, "costo": 1.2}),
	("2", "4", {"distancia": 0.5, "costo": 1.7}),
	("3", "4", {"distancia": 0.2, "costo": 1.0}),
	("3", "5", {"distancia": 0.7, "costo": 2.0}),
	("4", "5", {"distancia": 0.4, "costo": 1.4}),
])
# grafo.add_edges_from([
# 	("1", "2", {"distancia": 290, "costo": 2.9}),
# 	("2", "3", {"distancia": 650, "costo": 6.5}),
# 	("2", "5", {"distancia": 300, "costo": 3}),
# 	("3", "4", {"distancia": 1100, "costo": 11}),
# 	("4", "5", {"distancia": 550, "costo": 5.5}),
# 	("1", "5", {"distancia": 400, "costo": 4}),
# ])

# labeldict = {
# 	"1": "1. Plaza de España",
# 	"2": "2. Gran Vía",
# 	"3": "3. Sol",
# 	"4": "4. Puerta del Sol",
# 	"5": "5. Calle de Alcalá"
# }  #Consultado (06-2024) en: https://stackoverflow.com/questions/28533111/plotting-networkx-graph-with-node-labels-defaulting-to-node-name
labeldict = {
	"1": "1. Sol",
	"2": "2. Gran Vía",
	"3": "3. Tribunal",
	"4": "4. Plaza de España",
	"5": "5. Callao"
}  #Consultado (06-2024) en: https://stackoverflow.com/questions/28533111/plotting-networkx-graph-with-node-labels-defaulting-to-node-name

# Visualizar el grafo (opcional)
nx.draw(grafo, labels=labeldict, with_labels=True, node_size=500, node_color='skyblue', edge_color='black')
plt.show()

def mostrar_menu_desde():
	"""
	Función para mostrar el menú de opciones.
	"""
	print("Bienvenido al Sistema de Rutas de Madrid")
	print("Por favor, selecciona en donde te encuentras")
	# print("1. Plaza de España")
	# print("2. Gran Vía")
	# print("3. Sol")
	# print("4. Puerta del Sol")
	# print("5. Calle de Alcalá")
	print("1. Sol")
	print("2. Gran Vía")
	print("3. Tribunal")
	print("4. Plaza de España")
	print("5. Callao")
	print("6. Salir")

def mostrar_menu_hasta():
	"""
	Función para mostrar el menú de opciones.
	"""
	print("Por favor, selecciona a donde quieres ir")
	# print("1. Plaza de España")
	# print("2. Gran Vía")
	# print("3. Sol")
	# print("4. Puerta del Sol")
	# print("5. Calle de Alcalá")
	print("1. Sol")
	print("2. Gran Vía")
	print("3. Tribunal")
	print("4. Plaza de España")
	print("5. Callao")
	print("6. Salir")

def mostrar_menu_tipo_ruta():
	"""
	Función para mostrar el menú de opciones.
	"""
	print("Por favor, selecciona el tipo de ruta que deseas")
	print("1. Ruta más corta")
	print("2. Ruta más económica")
	print("3. Salir")

def seleccionar_opcion_desde():
	"""
	Función para obtener la selección del usuario.
	"""
	while True:
		try:
			opcion_desde = int(input("Ingrese su opción: "))
			if 1 <= opcion_desde <= 6:
				return opcion_desde
			else:
				print("Opción no válida. Intente nuevamente.")
		except ValueError:
			print("Por favor, escribe un número entero.")

def seleccionar_opcion_hasta():
	"""
	Función para obtener la selección del usuario.
	"""
	while True:
		try:
			opcion_hasta = int(input("Ingrese su opción: "))
			if 1 <= opcion_hasta <= 6:
				return opcion_hasta
			else:
				print("Opción no válida. Intente nuevamente.")
		except ValueError:
			print("Por favor, escribe un número entero.")

def seleccionar_opcion_tipo_ruta():
	"""
	Función para obtener la selección del usuario.
	"""
	while True:
		try:
			opcion_tipo_ruta = int(input("Ingrese su opción: "))
			if 1 <= opcion_tipo_ruta <= 3:
				return opcion_tipo_ruta
			else:
				print("Opción no válida. Intente nuevamente.")
		except ValueError:
			print("Por favor, escribe un número entero.")

def ejecutar_opcion_desde(opcion_desde):
	"""
	Función para ejecutar la acción correspondiente a la opción seleccionada.
	"""
	if opcion_desde == 1:
		# Implementar la acción de la Opción 1
		mostrar_menu_hasta()

		opcion_seleccionada_hasta = seleccionar_opcion_hasta()

		ejecutar_opcion_hasta(opcion_seleccionada_hasta)
		pass
	elif opcion_desde == 2:
		# Implementar la acción de la Opción 2
		mostrar_menu_hasta()

		opcion_seleccionada_hasta = seleccionar_opcion_hasta()

		ejecutar_opcion_hasta(opcion_seleccionada_hasta)
		pass
	elif opcion_desde == 3:
		# Implementar la acción de la Opción 3
		mostrar_menu_hasta()

		opcion_seleccionada_hasta = seleccionar_opcion_hasta()

		ejecutar_opcion_hasta(opcion_seleccionada_hasta)
		pass
	elif opcion_desde == 4:
		# Implementar la acción de la Opción 4
		mostrar_menu_hasta()

		opcion_seleccionada_hasta = seleccionar_opcion_hasta()

		ejecutar_opcion_hasta(opcion_seleccionada_hasta)
		pass
	elif opcion_desde == 5:
		# Implementar la acción de la Opción 5
		mostrar_menu_hasta()

		opcion_seleccionada_hasta = seleccionar_opcion_hasta()

		ejecutar_opcion_hasta(opcion_seleccionada_hasta)
		pass
	else:
		print("Saliendo del Sistema...")

def ejecutar_opcion_hasta(opcion_hasta):
	"""
	Función para ejecutar la acción correspondiente a la opción seleccionada.
	"""
	if opcion_hasta == 1:
		# Implementar la acción de la Opción 1
		mostrar_menu_tipo_ruta()

		opcion_seleccionada_tipo_ruta = seleccionar_opcion_tipo_ruta()

		ejecutar_opcion_tipo_ruta(opcion_hasta, opcion_seleccionada_tipo_ruta)
		pass
	elif opcion_hasta == 2:
		# Implementar la acción de la Opción 2
		mostrar_menu_tipo_ruta()

		opcion_seleccionada_tipo_ruta = seleccionar_opcion_tipo_ruta()

		ejecutar_opcion_tipo_ruta(opcion_hasta, opcion_seleccionada_tipo_ruta)
		pass
	elif opcion_hasta == 3:
		# Implementar la acción de la Opción 3
		mostrar_menu_tipo_ruta()

		opcion_seleccionada_tipo_ruta = seleccionar_opcion_tipo_ruta()

		ejecutar_opcion_tipo_ruta(opcion_hasta, opcion_seleccionada_tipo_ruta)
		pass
	elif opcion_hasta == 4:
		# Implementar la acción de la Opción 4
		mostrar_menu_tipo_ruta()

		opcion_seleccionada_tipo_ruta = seleccionar_opcion_tipo_ruta()

		ejecutar_opcion_tipo_ruta(opcion_hasta, opcion_seleccionada_tipo_ruta)
		pass
	elif opcion_hasta == 5:
		# Implementar la acción de la Opción 5
		mostrar_menu_tipo_ruta()

		opcion_seleccionada_tipo_ruta = seleccionar_opcion_tipo_ruta()

		ejecutar_opcion_tipo_ruta(opcion_hasta, opcion_seleccionada_tipo_ruta)
		pass
	else:
		print("Saliendo del menú...")

def ejecutar_opcion_tipo_ruta(opcion_hasta, opcion_tipo_ruta):
	"""
	Función para ejecutar la acción correspondiente a la opción seleccionada.
	"""
	if opcion_tipo_ruta == 1:
		# Implementar la acción de la Opción 1
		calcular_ruta_mas_corta(str(opcion_seleccionada_desde), str(opcion_hasta))
		pass
	elif opcion_tipo_ruta == 2:
		# Implementar la acción de la Opción 2
		calcular_ruta_mas_economica(str(opcion_seleccionada_desde), str(opcion_hasta))
		pass
	else:
		print("Saliendo del Menú...")

def calcular_ruta_mas_corta(nodo_inicial, nodo_destino):
	print("Calculando la ruta más corta...")
	ruta_corta = nx.shortest_path(grafo, source=nodo_inicial, target=nodo_destino, weight="distancia")
	distancia_total = nx.shortest_path_length(grafo, source=nodo_inicial, target=nodo_destino, weight="distancia")

	print(f"Ruta más corta entre {nodos[nodo_inicial]['nombre']} y {nodos[nodo_destino]['nombre']}:")
	for i in range(len(ruta_corta) - 1):
		nodo_actual = ruta_corta[i]
		nodo_siguiente = ruta_corta[i + 1]
		distancia = grafo[nodo_actual][nodo_siguiente]["distancia"]
		costo = grafo[nodo_actual][nodo_siguiente]["costo"]
		print(
			f"  - Ir de {nodos[nodo_actual]['nombre']} a {nodos[nodo_siguiente]['nombre']} ({distancia} km, {costo} €)")

	print(f"Distancia total: {distancia_total} km")
	print()

	# Dibujar el recorrido anterior entre los nodos
	grafo_ruta_corta = grafo.edge_subgraph(zip(ruta_corta, ruta_corta[1:]))
	posicion_nodos = nx.spring_layout(grafo)
	nx.draw_networkx(grafo, posicion_nodos, with_labels=True, node_size=500, node_color="lightblue")
	nx.draw_networkx_edges(grafo, posicion_nodos, edgelist=grafo_ruta_corta.edges(), edge_color="red", width=2)
	plt.show()

	input("Presiona Enter para continuar...")
	print()

def calcular_ruta_mas_economica(nodo_inicial, nodo_destino):
	print("Calculando la ruta más económica...")
	costo_economico = nx.shortest_path(grafo, source=nodo_inicial, target=nodo_destino, weight="costo")
	costo_total = nx.shortest_path_length(grafo, source=nodo_inicial, target=nodo_destino, weight="costo")

	print(f"Ruta más económica entre {nodos[nodo_inicial]['nombre']} y {nodos[nodo_destino]['nombre']}:")
	for i in range(len(costo_economico) - 1):
		nodo_actual = costo_economico[i]
		nodo_siguiente = costo_economico[i + 1]
		distancia = grafo[nodo_actual][nodo_siguiente]["distancia"]
		costo = grafo[nodo_actual][nodo_siguiente]["costo"]
		print(
			f"  - Ir de {nodos[nodo_actual]['nombre']} a {nodos[nodo_siguiente]['nombre']} ({distancia} km, {costo} €)")

	print(f"Costo total: {costo_total} €")
	print()

	# Dibujar el recorrido anterior entre los nodos
	grafo_ruta_economica = grafo.edge_subgraph(zip(costo_economico, costo_economico[1:]))
	posicion_nodos = nx.spring_layout(grafo)
	nx.draw_networkx(grafo, posicion_nodos, with_labels=True, node_size=500, node_color="lightblue")
	nx.draw_networkx_edges(grafo, posicion_nodos, edgelist=grafo_ruta_economica.edges(), edge_color="red", width=2)
	plt.show()

	input("Presiona Enter para continuar...")
	print()

while True:
	mostrar_menu_desde()
	opcion_seleccionada_desde = seleccionar_opcion_desde()
	ejecutar_opcion_desde(opcion_seleccionada_desde)
	if opcion_seleccionada_desde == 6:
		break
